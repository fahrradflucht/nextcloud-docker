FROM nextcloud:30-apache

RUN apt-get update && apt-get install -y \
    supervisor \
    libmagickcore-6.q16-6-extra \
  && rm -rf /var/lib/apt/lists/* \
  && mkdir /var/log/supervisord /var/run/supervisord

COPY supervisord.conf /

ENV NEXTCLOUD_UPDATE=1

# Services on TrueNAS
RUN groupmod -g 1001 www-data
RUN find / -group 33 -exec chgrp -h www-data {} \; || true
# nextcloud on TrueNAS
RUN usermod -u 1003 www-data
RUN find / -user 33 -exec chown -h www-data {} \; || true

CMD ["/usr/bin/supervisord", "-c", "/supervisord.conf"]

