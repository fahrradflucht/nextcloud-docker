IMAGE		= registry.gitlab.com/fahrradflucht/nextcloud-docker
GIT_TAG		= $(shell awk -F':' 'NR==1{print $$2}' Dockerfile)-cron
DOCKER_TAG	= $(shell git describe --always --tags --dirty)

.PHONY: all
all: tag build push

.PHONY: build
build:
	docker build --pull -t $(IMAGE):$(DOCKER_TAG) .

.PHONY: push
push:
	docker push $(IMAGE):$(DOCKER_TAG)

.PHONY: tag
tag:
	git tag -f $(GIT_TAG)
